import java.util.Random;

public class path {
    public static void main(String args[]){
        System.out.println("Here are a few nested if statements to test a path generation program.");

        Random rand = new Random();
        int a = rand.nextInt(100);

        while (a != 1){
            if (a % 2 == 0){
                a /= 2;
            } else {
                a *= 3;
                a += 1;
            }
        }

        /*
        if (a % 2 == 0){
            a /= 2;
            if(a % 3 < 1){
                a *= 3;
            } else {
                a /= 2;
            }
        } else {
            a *= 3;
            a++;
            if(a % 2 == 1){
                a -= 1;
            } else {
                a = a + 3;
            }
        }
        */
    }
}
