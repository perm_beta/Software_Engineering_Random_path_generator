import string
from Stack import *

if_whlile_stack = Stack()
braces_counter = 0
path_counter = 0

def main():
    print("Welcome to simple path generoator app: ")
    printNewLine()
    # filePath = input("Enter the name or the path of the JAVA file: ")
    filePath = "/Users/eyoel/Desktop/SE II/Projects/Lecture 6/path.java"
    print(filePath)
    parseDocument(filePath)


def parseDocument(path):
    
    braces_counter = 0

    openedFile = open(path)
    lines = openedFile.readlines()
    lines = [x.strip("\n") for x in lines]
    print(len(lines))
    for line in lines:
        if(checkIfLineContainsConditionalStatment(line)):
            if_whlile_stack.push(checkIfLineContainsConditionalStatment(line))
            if(line.find("{") != -1):
                braces_counter = braces_counter + 1
        else:

            print("No if")
    # print(lines)


def printNewLine():
    print("-----------------------------------------------------\n")


def checkIfLineContainsConditionalStatment(line):
    if (line.find("if") != -1):
        return "if"
    elif (line.find("while") != -1):
        return "while"
    elif (line.find("else") != -1):
        return "else"
    else:
        return False


main()
